# recipe-app-api-proxy

nginex proxy installation

## usage

## Environment Variables
 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 *  `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 *  `APP_PORT`- Port of the app to forward requests to (default:`9000`)

 ## Introduction ünitesinde technologies ve architecture overview dersleri genel bir kavrayış geliştirmek açısından önemli. Genel kavrayışın gelişmesi de ince noktalarda ne olduğunun anlaşılması ve ince noktaların akılda kalması açısından belirleyici. Detaylara girmeden önce bu high level bakış açısını iyice oturtmak ve arasıra tekrar gözden geçirmek şart.  
 
 ## aws kaynaklarını kullanmadan önce mutlaka hesapla. Her kaynağın kullanım ve fiyat politikalarını ayrı ayrı incele, optimizasyonları yap, aç-kapa. Introduction ünitesi, disclaimer dersini referans olarak kullan. 

 ## Unit 3 lesson 2; bir best practices ve iş akışı listesi oluşturmak iyi olur. Bu listeye bir de ilgili kaynak ve tutorialları eklersem epey bir işi kolaylaştırabilir. Genel iş akışı ve iş tanımları.
   ## aws ilk girişte multi faktor authentication'ı zorunlu yap. 
   ## iam user'larına billing information görme yetkisi ver. Herkes yaptığı harcamayı görsün ve harcamasından sorumlu olsun, şüpheli bir durum olduğunda erken farkedilsin. Bunun için kullanıcı adı --> my account --> scroll down to IAM user and Role access to billing information --> check Activate IAM Access box and update
   ## how to force new users to use multi factor authentication iam --> policies --> create policy --> json --> copy the policy in "aws self-manage credential with mfa" sample policy found through google search --> remove everthing in json tab and paste the policy --> click review policy and give it a name like "ForceMFA" and an optional description --> create
   ## how to create and assign users to this policy groups --> create a new group --> set a group name like "admins" --> next and attach policy to the group --> attach "administratorAccess from the aws list and from the one you created from the customer managed policies list --> next and review the summary --> create the group.
   ## how to create the user to use the group settings users --> create user -->give the user a name and set access rights console or programmatic access or both --> choose password generation method --> check "require password reset" box --> next to permissions --> add user to the group you have created before "admins" --> next to tags --> next to review summary --> create user --> user credential might be copied now or created again when necessary
   ## how to set mfa for the account: lesson 5 or google search. users will have readonly rights until they activate mfa and relogin with mfa. You can see everything but cannot change anything during readonly phase. iam --> users --> choose the user --> security credentials --> assign a mfa device by clicking manage --> choose device type like google authenticator --> read the qr code with the app --> enter the first two consequtive codes in the app-device --> assign mfa --> logout --> and use the mfa code for login
   ## aws vault users --> security credentials --> delete the old credentials and create a new one --> do not download security credentials but use them in the vault directly --> open up the console --> enter "aws-vault add 'userName'" --> enter access key id: copy and paste here --> enter your secret key: copy-paste here --> you will see "added credentials to ... "
   ## how to add mfa for aws-cli and aws-vault open up the console --> open up aws config file "vi ~/.aws/config" --> enter region "region=us-east1" --> enter mfa serial which is in user-security credentials-assigned mfa device "mfa_serial:arn:aws:iam:: ...", and copy-paste here --> save
   ## how to use mfa for aws-cli and aws-vault open up the console --> aws-vault exec user_name --duration=12h --cmd.exe --> enter and enter the mfa code --> enter the password for the vault
   ## how to set a budget click your name on upper left --> my billing information dashboard in the list --> budgets in the left side menu --> create a budget with fixed amount --> go down to configure alerts --> set an alarm and you can set different alarms at different points like %50, %80 etc --> confirm budget to review --> create Budget setting is not to stop the service but to be informed about it. You must stop the services manually if necessary.

## unit 4 setting up proxy
  ## lesson 1 Introduction to Nginx
